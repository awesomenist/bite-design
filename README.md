# README

### What is this repository for?

This repository has been created as a way of making sure everybody has the latest copy of Bite's app screens, as well as maintaining a better versioning of the development of the design. In the end, this should take out some of the confusion typically caused by handing over designs amongst designers and developers :)
>Version 1

### How do I get set up?

- You need the latest version of Sketch and Craft
- Download SkyFonts to get access to and maintain the fonts used
- 

### Contribution guidelines ###

* Try to make as detailed commit messages as possible.